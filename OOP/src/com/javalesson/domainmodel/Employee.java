package com.javalesson.domainmodel;

public class Employee {

    private static int id;
    private int employeeId;
    private String name;
    private String position;
    private int salary;
    private String dapartment;

    public Employee(String name, String position, int salary, String dapartment) {
        this.employeeId = id++;
        this.name = name;
        this.position = position;
        this.salary = salary;
        this.dapartment = dapartment;
    }

    public Employee(String name, String position, int salary) {
        new Employee(name, position, salary);
    }

    public Employee (String name, String position){
        this(name, position);
    }
}
