package com.javalesson.homework.lesson1;

public class Ex1 {
    public static void main(String[] args) {
        loop(0,2,10);
    }

    int cycle = 2;
    int a = 0;
    int b = 2;
    int n = 10;

    static void loop(int a, int b, int n) {
        for (int i = 0; i<n; i++ ){
            int result = a + b*n;
            System.out.println(result);
        }
        System.out.println();
    }
}

