package com.javalesson.controlStatement;

public class ForLoop {
    public static void main(String[] args) {
        int factorialLimit = 10;
        int factorial = 1;
        for (int i = 1; i <= factorialLimit; i++) {
            factorial *= i;
            System.out.println(i + " Factorial value = " + factorial);
        }
        System.out.println("final factorial value = " + factorial);

//        Prima metoda de decrementare prin ciclul for
        for (int i = 10; i >= 1; i--) {
            System.out.println("For I = " + i);
        }
//        A doua metoda de decrementare prin ciclul while
        int i = 10;
        while (i >= 1) {
            System.out.println("While I = " + i);
            i--;
        }
    }
}
