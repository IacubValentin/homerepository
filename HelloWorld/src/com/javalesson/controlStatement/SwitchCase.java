package com.javalesson.controlStatement;

public class SwitchCase {
    public static void main(String[] args) {

        int choise = 1;
        switch (choise) {
            case 1:
                System.out.println("You selected case 1");
                break;
            case 2:
                System.out.println("You selected case 2");
                break;
            case 3:
                System.out.println("You selected case 3");
                break;
            case 4:
                System.out.println("You selected case 4");
                break;
            default:
                System.out.println("You selected no active variant");
        }
        System.out.println("Final string");

        String dayOfTheWeek = "Monday";
        switch (dayOfTheWeek.toLowerCase()) {
            case "monday":
                System.out.println("Now is first day of the week!");
                break;
            case "tuesday":
                System.out.println("Now is second day of the week!");
                break;
            case "wednesday":
                System.out.println("Now is the third day of the week!");
                break;
            case "thursday":
                System.out.println("Now is the fourth day of the week!");
                break;
            case "friday":
                System.out.println("Now is the fifth day of the week!");
                break;
            case "saturday":
                System.out.println("Finally weekend, first weekend day");
                break;
            case "sunday":
                System.out.println("Last day of the weekend");
                break;
            default:
                System.out.println("Not a day of the week");
        }
        System.out.println("You can see the week day, below.");
    }
}
