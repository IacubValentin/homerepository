package com.javalesson.oop;

import java.util.SortedMap;

public class Dog {
    public static int dogCount;
    public static final int pawn = 4;
    public static final int tail = 1;
    private String name;
    private String breed;
    private Size size;

    Dog(){
        dogCount++;
        System.out.println("Dog's number is " + dogCount);
    }

    public static int getDogCount(){
        return dogCount;
    }

    public void barg() {
        switch (size) {
            case BIG:
            case VERY_BIG:
                System.out.println("Wof - Wof");
                break;
            case AVERAGE:
                System.out.println("Raf - Raf");
                break;
            case SMALL:
            case VERY_SMALL:
                System.out.println("Tiaf - Tiaf");
                break;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
    public void bite(){
        if (dogCount>2){
            System.out.println("Dog's are bite you");
        }
        else {barg();}
    }
}
