package com.javalesson.methods;

public class StringType {
    public static void main(String[] args) {
        String str1 = "I like coffee";
        System.out.println("Upper case for str1 = " + str1.toUpperCase());
        System.out.println("Lower case for str1 = " + str1.toLowerCase());

        String str2 = "I like coffee";
        String str3 = "I like coffee!!!";

        boolean b1 = str1 == str2;
        System.out.println("First boolean result = " + b1);

        String str4 = str3.substring(0, 13);
        boolean b2 = str1 == str4;
        System.out.println("Another boolean result = " + b2);

        String str5 = new String("I like coffee");
        boolean b3 = str1 == str5;
        System.out.println("Additional boolean result = " + b3);

        boolean b4 = str1.equals(str4);
        System.out.println("First right way to compare String => " + b4);

        boolean b5 = str1.equals(str5);
        System.out.println("Addition right way to compare String => " + b5);

        boolean b6 = str1.startsWith("I like");
        System.out.println(b6);
        boolean b7 = str1.startsWith(" like", 1);
        System.out.println(b7);

        boolean b8 = str1.endsWith("coffee");
        System.out.println("b8 = " + b8);
        boolean b9 = str1.endsWith("eee");
        System.out.println("b9 = " + b9);

        System.out.println(str3.replace("like", "like soo much black"));

        System.out.println("Index of char c = " + str1.indexOf("c"));
        System.out.println("Index of no found char in String = " + str1.indexOf("abc"));



    }

}

