package com.javalesson.oop;

public class Main {
    public static void main(String[] args) {
        Dog dog1 = new Dog();
        dog1.setBreed("Labrador");
        dog1.setName("Mike");
        dog1.setSize(Size.AVERAGE);
        dog1.barg();


        Dog dog2 = new Dog();
        dog2.setBreed("Doberman");
        dog2.setName("Charley");
        dog2.setSize(Size.SMALL);
        dog2.barg();

    }
}
