package com.javalesson.operators;

public class Operators {
    public static void main(String[] args) {
//      + - / * %
        int a = 10;
        int b = 3;

        int c = a + b;
        int d = a - b;

        int e = a * b;
        int f = a / b;

        int g = a % b;

        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        System.out.println("d = " + d);
        System.out.println("e = " + e);
        System.out.println("f = " + f);
        System.out.println("g = " + g);

        a = a + 5;
        System.out.println("New a = " + a);
        a += 5;
        System.out.println("New one a = " + a);

        b++;
        System.out.println("New b = " + b);

        --b;
        System.out.println("New one b = " + b);

        int n = 7;
        int m = 7;

        int result1 = 2*n++;
        int result2 = 2*++m;

        System.out.println("result1 = " + result1);
        System.out.println("result2 = " + result2);

//      == != < > <= >= && || ?:

    int x = 3;
    int y = 5;
    int z = 8;

    boolean boolVal = x==y;
        System.out.println("boolVal = "+ boolVal);
//        true && true = true
//        true && false = false
//        true || false = true
//        false || false = false


    }
}