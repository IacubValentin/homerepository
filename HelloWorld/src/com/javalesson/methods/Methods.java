package com.javalesson.methods;

public class Methods {
    public static void main(String[] args) {
        printMessage("Alex");
        calcRectangleSquare(2, 4);
        int square = anotherCalcRectangleSquare(3, 6);
        System.out.println("Square = " + square);
        int calcSquare = calcSquare(8);
        System.out.println("CalcSquare = " + calcSquare);

        int sumOfSquares = square + calcSquare;
        System.out.println("SumOfSquares1 = " + sumOfSquares);
        System.out.println("SumOfSquares2 = " + square + calcSquare); //Asa este gresit => String + int+ int => String
        System.out.println("SumOfSquares3 = " + (square + calcSquare)); //Asa este corect => String + (int+ int) => String + int

        //Mai jos este cea mai optima varianta celui de mai sus model
        printMessage("Alex");
        System.out.println("Square = " + anotherCalcRectangleSquare(3, 6));
        System.out.println("CalcSquare = " + calcSquare(8));
        System.out.println("SumOfSquares3 = " + (anotherCalcRectangleSquare(3, 6) + calcSquare(8)));



    }

    static void printMessage(String name) {
        System.out.println("Hello " + name);
    }

    static void calcRectangleSquare(int x, int y) {
        int rectangleSquare = x * y;
        System.out.println("RectangleSquare = " + rectangleSquare);
    }

    static int anotherCalcRectangleSquare(int x, int y) {
        int rectangleSquare = x * y;
        return rectangleSquare;
    }

    static int calcSquare(int x) {
        return x * x;
    }
}
